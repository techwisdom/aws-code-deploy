#!/usr/bin/env bats

set -e

setup() {
  # Create a unique application and deployment
  RANDOM_NUMBER=$RANDOM
  APPLICATION_NAME="bbci-code-deploy-test"
  DEPLOYMENT_GROUP="bbci-code-deploy-test-group"
}

teardown() {
  rm -rf ${BATS_TEST_DIRNAME}/tmp
}

@test "should upload and create a revision for a zip file to a custom S3 bucket"  {
    # Copy files into location and build a zip file.
    mkdir ${BATS_TEST_DIRNAME}/tmp
    cp -r ${BATS_TEST_DIRNAME}/app/* "${BATS_TEST_DIRNAME}/tmp"
    cd ${BATS_TEST_DIRNAME}/tmp
    echo '{"message":'${RANDOM_NUMBER}'}' > ${BATS_TEST_DIRNAME}/tmp/config.json
    zip -r myapp.zip *

    # Run pipe
    run docker run \
      -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
      -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e ZIP_FILE="${BATS_TEST_DIRNAME}/tmp/myapp.zip" \
      -e S3_BUCKET="bbci-code-deploy-demo" \
      -e COMMAND="upload" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "0" ]]
    [[ "${output}" =~ "Application uploaded and revision created" ]]
}

@test "should upload and create a revision for a zip file" {
    # Copy files into location and build a zip file.
    mkdir ${BATS_TEST_DIRNAME}/tmp
    cp -r ${BATS_TEST_DIRNAME}/app/* "${BATS_TEST_DIRNAME}/tmp"
    cd ${BATS_TEST_DIRNAME}/tmp
    echo '{"message":'${RANDOM_NUMBER}'}' > ${BATS_TEST_DIRNAME}/tmp/config.json
    zip -r myapp.zip *

    # Run pipe
    run docker run \
      -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
      -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
      -e AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION} \
      -e APPLICATION_NAME="${APPLICATION_NAME}" \
      -e ZIP_FILE="${BATS_TEST_DIRNAME}/tmp/myapp.zip" \
      -e COMMAND="upload" \
      -v $(pwd):$(pwd) \
      -w $(pwd) \
      bitbucketpipelines/aws-code-deploy

    [[ "${status}" == "0" ]]
    [[ "${output}" =~ "Application uploaded and revision created" ]]
}
