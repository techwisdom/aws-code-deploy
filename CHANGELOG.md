# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.6

- patch: Update aws-cli base docker image version.
- patch: Update pipes bash toolkit version.

## 0.2.5

- patch: Fixed the issue with deploying large projects

## 0.2.4

- patch: Minor documentations updates

## 0.2.3

- patch: Fixed the bug that caused successfull builds to crash

## 0.2.2

- patch: Updated contributing guidelines

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Add support for DEBUG parameter.
- minor: Rename from tasks to pipes.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines AWS CodeDeploy deployment pipe.

